// Import FirebaseAuth and firebase.
import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase';
import {Segment,Header,Button,Image,Feed,Icon,Grid,Form} from 'semantic-ui-react';
import config from './config';

// Configure Firebase.

firebase.initializeApp(config);

let database = firebase.database();
let dbRef = database.ref('messages');

export default class App extends React.Component {

  // The component's Local state.
  state = {
    isSignedIn: false,
    messages: []// Local signed-in state.
  };

  // Configure FirebaseUI.
  uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'popup',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccessWithAuthResult: () => false
    }
  };

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
        (user) => this.setState({isSignedIn: !!user,user:firebase.auth().currentUser})
    );
    let obj = {};
    dbRef.on('value', snapshot => {
      obj = {...snapshot.val()};
      this.setState({messages:Object.keys(obj).map(key => obj[key])});
      //this.setState({messages:Object.keys(snapshot.val()).map(key => snapshot.val()[key])});
    })
  }
  
  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  handleChange = (event) => {
    let {name,value} = event.target;
    this.setState({[name]:value});
  }
  
  send = () => {
    dbRef.push({
      message:this.state.message,
      photoURL:firebase.auth().currentUser.photoURL,
      displayName:firebase.auth().currentUser.displayName
    })
  }

  render() {
    if (!this.state.isSignedIn) {
      return (
        <Segment>
        <div style={{textAlign: 'center'}}>
          <h1>Tadka Live</h1>
          <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()}/>
        </div>
        </Segment>
      );
    }
    return (
      <Segment>
        <Header as="h1" style={{textAlign:'center'}}>Tadka Live</Header>
        <Button onClick={() => firebase.auth().signOut()} primary>Sign-out</Button>
        <p style={{textAlign:'center'}}>Welcome {firebase.auth().currentUser.displayName}! You are now signed-in!</p>
        
        <Grid columns={1} style={{width:'500px'}}>
          <Grid.Column>
            <Feed style={{height:'300px', overflow:'scroll'}}>
            
            {this.state.messages.map(msg => {
            
            return <Feed.Event key={msg.message}>
                <Feed.Label image={msg.photoURL} />
                <Feed.Content>
                  <Feed.Summary>
                    <a>{msg.displayName}</a>
                  </Feed.Summary>
                  <Feed.Extra text>
                    {msg.message}
                  </Feed.Extra>
                  <Feed.Meta>
                    <Feed.Like>
                      <Icon name='like' />
                      5 Likes
                    </Feed.Like>
                  </Feed.Meta>
                </Feed.Content>
              </Feed.Event>})}
            
            </Feed>
            
            <Form onSubmit={this.send}>
            <Form.Field>
              <input placeholder="msg" onChange={this.handleChange} name="message"/>
            </Form.Field>
            <Button type="submit" content="send"/>
          </Form>
          </Grid.Column>
        </Grid>
        
      </Segment>
    );
  }
}